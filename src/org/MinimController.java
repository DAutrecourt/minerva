package org;

import ddf.minim.AudioOutput;
import ddf.minim.Minim;
import java.math.BigInteger;

/**
 * Created by Nick on 7/31/2014.
 *
 */

class MinimController extends Minim {
    private AudioOutput out;
    public boolean isPaused;

    MinimController(Object o) {
        super(o);
        Minim minim = new Minim(this);
        out = minim.getLineOut();
        isPaused = false;
    }

    public void playNotes(BigInteger[] notes, float offset, float length, float tempo, boolean separateTriads){
        out.pauseNotes();
        out.setTempo(tempo);
        float timeStamp = 0.0f;
        int triad = 0;
        for (BigInteger Note: notes){
            out.playNote(timeStamp, length, transmuteNote(Note));
            if (separateTriads && triad < 2){
                triad += 1;
                timeStamp+=offset;
            }else if (separateTriads && triad == 2){
                triad = 0;
                timeStamp += offset + length;
            }
            else timeStamp += offset;
        }
        out.resumeNotes();
    }

    public void playChords(BigInteger[] notes, float offset){
        out.pauseNotes();
        float timeStamp  = 0.0f;
        String[] transNotes = transmuteChord(notes);
        for (String note: transNotes){
            out.playNote(timeStamp,note);
            timeStamp+=offset;
        }
        out.resumeNotes();
    }

    protected String[] transmuteChord(BigInteger[] notes) {
        String[] outNotes = new String[notes.length];
        for (int i = 0; i<notes.length; i++){
            outNotes[i] = transmuteNote(notes[i]);
        }
        return outNotes;
    }

    protected String transmuteNote(BigInteger Note){
        String transmuteResult;
        int shift = Note.remainder(new BigInteger("12")).intValue();
        switch(shift){
            case 1: transmuteResult = "C5";
                break;
            case 2: transmuteResult = "Db5";
                break;
            case 3: transmuteResult = "D5";
                break;
            case 4: transmuteResult = "Eb5";
                break;
            case 5: transmuteResult = "E5";
                break;
            case 6: transmuteResult = "F5";
                break;
            case 7: transmuteResult = "Gb5";
                break;
            case 8: transmuteResult = "G5";
                break;
            case 9: transmuteResult = "Ab5";
                break;
            case 10: transmuteResult = "A5";
                break;
            case 11: transmuteResult = "Bb5";
                break;
            case 0:  transmuteResult = "B5";
                break;
            default: transmuteResult = "error";
                break;
        }
        return transmuteResult;
    }

    public void playTestTone() {
        out.setTempo(80);
        out.pauseNotes();
        out.playNote(0.0f, 2.0f, "C5");
        out.playNote(0.5f, 1.5f, "E5");
        out.playNote(1.0f, 1.0f, "G5");
        out.resumeNotes();
    }
}
