package org;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by Nick on 7/18/2014.
 *
 * Minerva | Transpose numbers into notes, play notes, export .abc files of those notes.
 *
 * Exit codes:
 *  0 - success
 *  1 - generic error
 *  2 - load error
 *  3 - write error
 */

public class Main extends JFrame implements ActionListener{
    private boolean DEBUG = false;
    private MinimController MinimControl;

    // MinimControl options
    public BigInteger[] notes;
    public float offset;
    public float length;
    public float tempo;
    public boolean separate;
    public int mode;

    //GUI
    private JButton playButton;
    private JComboBox<String> modeSelect;
    private JButton open;
    private JTextField filename;
    private JTextField dir;
    private JTextArea log;
    private JButton export;

    public static void main(String[] args){
        new Main();
    }

    public Main(){
        MinimControl = new MinimController(this);

        notes = new BigInteger[]{};
        offset = 0.1f;
        length = 1.0f;
        tempo = 80f;
        separate = true;
        mode = 0;

        frameBuild();
    }

    private void frameBuild() {
        this.getContentPane().setLayout(new FlowLayout());
        setTitle("Minerva");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocation(90,120);

        playButton = new JButton("Play");
        playButton.addActionListener(this);
        add(playButton);

        String[] Modes = new String[]{"Notes", "Chords", "Test Tone"};
        modeSelect = new JComboBox<String>();
        for (String s: Modes) modeSelect.addItem(s);
        modeSelect.addActionListener(this);
        add(modeSelect);

        open = new JButton("Open");
        open.addActionListener(this);
        add(open);

        filename = new JTextField();
        filename.setText("Filename");
        filename.setEditable(false);
        filename.setPreferredSize(new Dimension(290,20));
        add(filename);

        dir = new JTextField();
        dir.setText("Directory");
        dir.setEditable(false);
        dir.setPreferredSize(new Dimension(290,20));
        add(dir);

        JLabel logLabel = new JLabel("log:");
        add(logLabel);

        log = new JTextArea();
        log.setPreferredSize(new Dimension(290,100));
        log.setEditable(false);
        add(log);

        export = new JButton("Export to abc");
        export.addActionListener(this);
        add(export);

        this.setPreferredSize(new Dimension(300,300));
        pack();
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object action = e.getSource();
        if (action == playButton){
            switch (mode){
                case 0: MinimControl.playChords(notes, offset);
                    break;
                case 1: MinimControl.playNotes(notes, offset, length, tempo, separate);
                    break;
                case 2: MinimControl.playTestTone();
                    break;
                default: debug("Mode selection failed");
                    break;
            }
        }
        if (action == modeSelect){
            switch (modeSelect.getSelectedIndex()){
                case 0: mode = 0;
                    break;
                case 1: mode = 1;
                    break;
                case 2: mode = 2;
                    break;
                default: mode = -1;
                    break;
            }
        }
        if (action == open){
            JFileChooser c = new JFileChooser();
            int rVal = c.showOpenDialog(this);
            if (rVal == JFileChooser.APPROVE_OPTION) {
                String[] split = c.getSelectedFile().getName().split("\\.");
                System.out.println(split[split.length-1]);
                if (split[split.length-1].equalsIgnoreCase("txt")){
                    try{
                        Path p = Paths.get(c.getSelectedFile().getPath());
                        readFile(p);
                        filename.setText(c.getSelectedFile().getName());
                        dir.setText(c.getCurrentDirectory().toString());
                        log.append("Loaded file: " + filename.getText() + ".\r\n");
                    }catch(Exception ioe){
                        log.append("Failed to load file.\r\n");
                        ioe.printStackTrace();
                    }
                }
            }
        }
        if (action == export) try {
            MakeABC();
        } catch (FileNotFoundException e1) {
            exit(2);
        } catch (UnsupportedEncodingException e1) {
            exit(3);
        }
    }

    //Files
    private void readFile(Path path) throws IOException{
        java.util.List<String> lines = Files.readAllLines(path, Charset.defaultCharset());
        ArrayList<BigInteger> parsed = new ArrayList<BigInteger>();
        for (String s: lines) {
            String[] notesList = s.split(" ");
            for (String q : notesList) {
                try{
                    parsed.add(new BigInteger(q));
                }catch(Exception nfe){
                    log.append("Something has gone horribly wrong with the file. Please make sure you've written it correctly");
                    exit(1);
                }
            }
        }
        notes = new BigInteger[parsed.size()];
        for (int i=0; i<parsed.size(); i++){
            notes[i] = parsed.get(i);
        }
    }

    private void MakeABC() throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter pw = new PrintWriter("Translation.abc", "UTF-8");
        pw.append("X:1\r\nT:Minerva File Output\r\nM:4/4\r\nC:Trad\r\nK:C\r\nL:1/2\r\n");
        int me = 0;
        int li = 0;
        for (BigInteger b: notes){
            String s = MinimControl.transmuteNote(b);
            if (s.length() == 2) s=s.substring(0,1);
            else if (s.length()==3) s="_"+s.substring(0,1);
            if (me == 4) {
                pw.append(" | ");
                me=0;
                li++;
            }
            if (li == 4){
                pw.append("\r\n");
                li = 0;
            }
            pw.append(s);
            me++;
        }
        pw.append("|]");
        pw.close();
        log.append("Wrote Translation.abc in PWD");
    }

    //Utilities
    public void exit(int code){
        MinimControl.stop();
        System.exit(code);
    }
    public void debug(String s){
        if (DEBUG)
            System.out.println("Minerva: " + s);
    }
}
